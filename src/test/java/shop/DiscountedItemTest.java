package shop;

import cz.cvut.eshop.shop.DiscountedItem;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class DiscountedItemTest {
    @Test
    public void testDiscountedItemConstructorFromDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2018);
        cal.set(Calendar.MONTH, Calendar.OCTOBER);
        cal.set(Calendar.DAY_OF_MONTH, 26);
        Date dateBegin = cal.getTime();
        cal.set(Calendar.YEAR, 2019);
        cal.set(Calendar.MONTH, Calendar.OCTOBER);
        cal.set(Calendar.DAY_OF_MONTH, 26);
        Date dateEnd = cal.getTime();

        DiscountedItem itemFromDate = new DiscountedItem(1,"test",1.0f,"food",20, dateBegin, dateEnd);
        assertNotNull(itemFromDate);
    }

    @Test
    public void testDiscontinuedItemConstructorFromString() {
        DiscountedItem itemFromString = new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019");
        assertNotNull(itemFromString);
    }

    @Test
    public void testDiscountOfExistingItem() {
        DiscountedItem item = new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019");
        DiscountedItem item2 = new DiscountedItem(1,"test",1.0f,"food",0, "0.0.0", "0.0.0");
        item2.setDiscount(20);
        item2.setDiscountFrom("22.10.2018");
        item2.setDiscountTo("22.10.2019");

        assertEquals(item.toString(),item2.toString());
        assertEquals(true, item.equals(item2));
        assertEquals(true, item2.equals(item));
    }

    @Test
    public void testCopyOfExistingDiscountedItem() {
        DiscountedItem item = new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019");
        DiscountedItem item2 = item.copy();
        assertEquals(item.getID(), item2.getID());
        assertEquals(item.getName(), item2.getName());
        assertEquals(item.getPrice(), item2.getPrice(), 0.5f);
        assertEquals(item.getCategory(), item2.getCategory());
        assertEquals(item.getDiscount(), item2.getDiscount());
        assertEquals(item.getDiscountFrom(), item2.getDiscountFrom());
        assertEquals(item.getDiscountTo(), item2.getDiscountTo());
        assertEquals(true, item.equals(item2));
        assertEquals(true, item2.equals(item));
    }

    @Test
    public void testCompareTwoDiscountedItemsWithDifferentDiscount() {
        DiscountedItem item = new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019");
        DiscountedItem item2 = item.copy();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2018);
        cal.set(Calendar.MONTH, Calendar.OCTOBER);
        cal.set(Calendar.DAY_OF_MONTH, 26);
        Date date = cal.getTime();
        item2.setDiscountTo(date);
        assertEquals(false, item2.equals(item));
    }

    @Test
    public void testCompareDiscountedItemWithStandardItemOfSameSpecs() {
        DiscountedItem item = new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019");
        StandardItem item2 = new StandardItem(1, "test", 1.0f, "food", 6);
        assertEquals(false, item.equals(item2));
    }

    @Test
    public void testDataParsingError() {
        DiscountedItem item = new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019");
        assertEquals(null, item.parseDate("11/05/1990"));

    }

    @Test
    public void testCommonItemGettersSetters() {
        DiscountedItem item = new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019");
        item.setID(2);
        assertEquals(2, item.getID());

        item.setName("test2");
        assertEquals("test2", item.getName());

        item.setPrice(5.0f);
        assertEquals(5.0f, item.getPrice(), 0.1f);

        item.setCategory("drinks");
        assertEquals("drinks", item.getCategory());
    }
}
