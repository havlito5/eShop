package shop;

import cz.cvut.eshop.shop.DiscountedItem;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StandardItemTest {
    @Test
    public void testStandardItemConstructor() {
        StandardItem item = new StandardItem(1,"test",1.0f,"food",20);
        assertNotNull(item);
    }

    @Test
    public void testCopyOfExistingStandardItem() {
        StandardItem item = new StandardItem(1,"test",1.0f,"food",20);
        StandardItem item2 = item.copy();
        assertEquals(item.getID(), item2.getID());
        assertEquals(item.getName(), item2.getName());
        assertEquals(item.getPrice(), item2.getPrice(), 0.5f);
        assertEquals(item.getCategory(), item2.getCategory());
        assertEquals(item.getLoyaltyPoints(), item2.getLoyaltyPoints());
        assertEquals(true, item.equals(item2));
        assertEquals(true, item2.equals(item));
    }

    @Test
    public void testCompareStandardItemWithDiscountedItemOfSameSpecs() {
        StandardItem item = new StandardItem(1, "test", 1.0f, "food", 6);
        DiscountedItem item2 = new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019");
        assertEquals(false, item.equals(item2));
    }

    @Test
    public void testSettingLoyaltyPoints() {
        StandardItem item = new StandardItem(1, "test", 1.0f, "food", 6);
        item.setLoyaltyPoints(4);
        assertEquals(4, item.getLoyaltyPoints());
    }

    @Test
    public void testStringOutput() {
        StandardItem item = new StandardItem(1, "test", 1.0f, "food", 6);
        assertNotNull(item.toString());
    }

    @Test
    public void testCommonItemGettersSetters() {
        StandardItem item = new StandardItem(1, "test", 1.0f, "food", 6);
        item.setID(2);
        assertEquals(2, item.getID());

        item.setName("test2");
        assertEquals("test2", item.getName());

        item.setPrice(5.0f);
        assertEquals(5.0f, item.getPrice(), 0.1f);

        item.setCategory("drinks");
        assertEquals("drinks", item.getCategory());
    }
}
