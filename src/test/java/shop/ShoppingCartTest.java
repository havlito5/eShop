package shop;

import cz.cvut.eshop.shop.DiscountedItem;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ShoppingCartTest {
    @Test
    public void testShoppingCartConstructorEmpty() {
        ShoppingCart cart = new ShoppingCart();
        assertNotNull(cart);
    }

    @Test
    public void testShoppingCartConstructorFromList() {
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019"));
        items.add(new StandardItem(1, "test", 1.0f, "food", 6));

        ShoppingCart cart = new ShoppingCart(items);
        assertNotNull(cart);
    }

    @Test
    public void testAddingItemToCart() {
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019"));
        items.add(new StandardItem(1, "test", 1.0f, "food", 6));

        ShoppingCart cart = new ShoppingCart(items);
        StandardItem newItem = new StandardItem(2, "test2", 2.0f, "candy", 5);
        cart.addItem(newItem);

        items.add(newItem);
        assertEquals(items, cart.getCartItems());
        //assertEquals(3, cart.getItemsCount());
    }

    @Test
    public void testRemovingItemFromCart() {
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new DiscountedItem(1,"test",1.0f,"food",20, "22.10.2018", "22.10.2019"));
        items.add(new StandardItem(1, "test", 1.0f, "food", 6));

        ShoppingCart cart = new ShoppingCart(items);
        cart.removeItem(1);

        assertEquals(0, cart.getItemsCount());
    }

    @Test
    public void testTotalPriceTally() {
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new DiscountedItem(1,"test",20.0f,"food",20, "22.10.2018", "22.10.2019"));
        items.add(new StandardItem(1, "test", 5.0f, "food", 6));
        ShoppingCart cart = new ShoppingCart(items);

        assertEquals(25.0f, cart.getTotalPrice(), 0.1f);
    }

    @Test
    public void testTotalPriceTallyForEmptyCart() {
        ShoppingCart cart = new ShoppingCart();
        assertEquals(0.0f, cart.getTotalPrice(), 0.1f);
    }
}
